//
//  ViewController.m
//  TestLRU
//
//  Created by Dushyant Bansal on 4/21/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import "ViewController.h"
#import "LRUCache.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)testLRUCache
{
  LRUCache *cache = [[LRUCache alloc] initWithSize:5];
  for (int id=0; id<10; id++) {
    Book *book = [[Book alloc] init];
    book.id = [NSNumber numberWithInt:id];
    [cache insertBook:book];
  }
  Book *book = [[Book alloc] init];
  book.id = [NSNumber numberWithInt:3];
  [cache insertBook:book];
  
  [cache print];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  [self testLRUCache];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

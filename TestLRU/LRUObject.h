//
//  LRUObject.h
//  TestLRU
//
//  Created by Dushyant Bansal on 4/21/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Book.h"

@interface LRUObject : NSObject
@property (nonatomic) LRUObject *next;
@property (nonatomic) LRUObject *previous;
@property (nonatomic) Book *book;
@end

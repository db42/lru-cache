//
//  LRUCache.h
//  TestLRU
//
//  Created by Dushyant Bansal on 4/21/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Book.h"
#import "LRUObject.h"

@interface LRUCache : NSObject

- (instancetype)initWithSize:(NSUInteger)size;
- (void)insertBook:(Book *)book;
- (void)print;
@end

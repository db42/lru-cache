//
//  LRUCache.m
//  TestLRU
//
//  Created by Dushyant Bansal on 4/21/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import "LRUCache.h"

@interface LRUCache ()
@property (nonatomic) NSMutableDictionary *hashMap;

@property (nonatomic) LRUObject *head;
@property (nonatomic) LRUObject *end;

@property (nonatomic) NSUInteger maxSize;
@property (nonatomic) NSUInteger curSize;

@end

@implementation LRUCache
- (instancetype)initWithSize:(NSUInteger)size {
  self = [self init];
  if (self) {
    _maxSize = size;
  }
  return self;
}

- (void)insertBook:(Book *)book {
  LRUObject *bookLRUObject;
  if ([self isAlreadyCached:book]) {
    bookLRUObject = (LRUObject *)[self.hashMap objectForKey:book.id];
    [self removeFromList:bookLRUObject];
    [self appendToList:bookLRUObject];
  } else {
    bookLRUObject = [[LRUObject alloc] init];
    bookLRUObject.book = book;
    
    [self appendToList:bookLRUObject];
    //update hashMap
    [self.hashMap setObject:bookLRUObject forKey:bookLRUObject.book.id];
    
    if (self.curSize > self.maxSize) {
      LRUObject *objectToRemove = self.head;
      [self.hashMap removeObjectForKey:objectToRemove.book.id];
      [self removeFromList:objectToRemove];
    }
  }
}

- (BOOL)isAlreadyCached:(Book *)book {
  id bookLRUObject = [self.hashMap objectForKey:book.id];
  return (bookLRUObject != nil);
}

- (void)appendToList:(LRUObject *)bookLRUObject {
  bookLRUObject.next = nil;
  if (self.head == nil) {
    self.head = bookLRUObject;
    self.head.previous = nil;
    self.head.next = nil;
  }
  
  LRUObject *oldTail = self.end;
  
  self.end = bookLRUObject;
  oldTail.next = self.end;
  self.end.previous = oldTail;
  
  self.curSize ++;
}

- (void)removeFromList:(LRUObject *)bookLRUObject {
  if (bookLRUObject.previous == nil) {
    //update head
    self.head = bookLRUObject.next;
    self.head.previous = nil;
  }
  if (bookLRUObject.next == nil) {
    //update tail
    self.end = bookLRUObject.previous.next;
    self.end.next = nil;
  }
  
  bookLRUObject.previous.next = bookLRUObject.next;
  self.curSize --;
}

- (void)print {
  LRUObject *head = self.head;
  while (head != nil) {
    NSLog(@"%@\n", head.book.id);
    head = head.next;
  }
  NSLog(@"size %lu\n", (unsigned long)self.curSize);
}

- (NSMutableDictionary *)hashMap {
  if (_hashMap == nil) {
    _hashMap = [[NSMutableDictionary alloc] init];
  }
  return _hashMap;
}

@end

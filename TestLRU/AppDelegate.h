//
//  AppDelegate.h
//  TestLRU
//
//  Created by Dushyant Bansal on 4/21/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
